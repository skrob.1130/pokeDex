void drawChar(char letter, int x, int y) {
  switch(letter) {
  case 'あ':
    drawBox(H_A, x, y);
    break;

  case 'い':
    drawBox(H_I, x, y);
    break;

  case 'う':
    drawBox(H_U, x, y);
    break;

  case 'え':
    drawBox(H_E, x, y);
    break;

  case 'お':
    drawBox(H_O, x, y);
    break;

  case 'か':
    drawBox(H_KA, x, y);
    break;

  case 'き':
    drawBox(H_KI, x, y);
    break;

  case 'く':
    drawBox(H_KU, x, y);
    break;

  case 'け':
    drawBox(H_KE, x, y);
    break;

  case 'こ':
    drawBox(H_KO, x, y);
    break;

  case 'さ':
    drawBox(H_SA, x, y);
    break;

  case 'し':
    drawBox(H_SI, x, y);
    break;

  case 'す':
    drawBox(H_SU, x, y);
    break;

  case 'せ':
    drawBox(H_SE, x, y);
    break;

  case 'そ':
    drawBox(H_SO, x, y);
    break;

  case 'た':
    drawBox(H_TA, x, y);
    break;

  case 'ち':
    drawBox(H_TI, x, y);
    break;

  case 'つ':
    drawBox(H_TU, x, y);
    break;

  case 'て':
    drawBox(H_TE, x, y);
    break;

  case 'と':
    drawBox(H_TO, x, y);
    break;

  case 'な':
    drawBox(H_NA, x, y);
    break;

  case 'に':
    drawBox(H_NI, x, y);
    break;

  case 'ぬ':
    drawBox(H_NU, x, y);
    break;

  case 'ね':
    drawBox(H_NE, x, y);
    break;

  case 'の':
    drawBox(H_NO, x, y);
    break;

  case 'は':
    drawBox(H_HA, x, y);
    break;

  case 'ひ':
    drawBox(H_HI, x, y);
    break;

  case 'ふ':
    drawBox(H_HU, x, y);
    break;

  case 'へ':
    drawBox(H_HE, x, y);
    break;

  case 'ほ':
    drawBox(H_HO, x, y);
    break;

  case 'ま':
    drawBox(H_MA, x, y);
    break;

  case 'み':
    drawBox(H_MI, x, y);
    break;

  case 'む':
    drawBox(H_MU, x, y);
    break;

  case 'め':
    drawBox(H_ME, x, y);
    break;

  case 'も':
    drawBox(H_MO, x, y);
    break;

  case 'や':
    drawBox(H_YA, x, y);
    break;

  case 'ゆ':
    drawBox(H_YU, x, y);
    break;

  case 'よ':
    drawBox(H_YO, x, y);
    break;

  case 'ら':
    drawBox(H_RA, x, y);
    break;

  case 'り':
    drawBox(H_RI, x, y);
    break;

  case 'る':
    drawBox(H_RU, x, y);
    break;

  case 'れ':
    drawBox(H_RE, x, y);
    break;

  case 'ろ':
    drawBox(H_RO, x, y);
    break;

  case 'わ':
    drawBox(H_WA, x, y);
    break;

  case 'を':
    drawBox(H_WO, x, y);
    break;

  case 'ゃ':
    drawBox(H_LYA, x, y);
    break;

  case 'ゅ':
    drawBox(H_LYU, x, y);
    break;

  case 'ょ':
    drawBox(H_LYO, x, y);
    break;

  case 'ぁ':
    drawBox(H_LTU, x, y);
    break;

  case 'ぃ':
    drawBox(H_NN, x, y);
    break;

  case 'ぅ':
    drawBox(H_LTU, x, y);
    break;

  case 'ぇ':
    drawBox(H_NN, x, y);
    break;

  case 'ぉ':
    drawBox(H_LTU, x, y);
    break;

  case 'ゕ':
    drawBox(H_NN, x, y);
    break;

  case 'っ':
    drawBox(H_LTU, x, y);
    break;

  case 'ゖ':
    drawBox(H_LTU, x, y);
    break;

  case 'ん':
    drawBox(H_NN, x, y);
    break;



  case 'ア':
    drawBox(K_A, x, y);
    break;

  case 'イ':
    drawBox(K_I, x, y);
    break;

  case 'ウ':
    drawBox(K_U, x, y);
    break;

  case 'エ':
    drawBox(K_E, x, y);
    break;

  case 'オ':
    drawBox(K_O, x, y);
    break;

  case 'カ':
    drawBox(K_KA, x, y);
    break;

  case 'キ':
    drawBox(K_KI, x, y);
    break;

  case 'ク':
    drawBox(K_KU, x, y);
    break;

  case 'ケ':
    drawBox(K_KE, x, y);
    break;

  case 'コ':
    drawBox(K_KO, x, y);
    break;

  case 'サ':
    drawBox(K_SA, x, y);
    break;

  case 'シ':
    drawBox(K_SI, x, y);
    break;

  case 'ス':
    drawBox(K_SU, x, y);
    break;

  case 'セ':
    drawBox(K_SE, x, y);
    break;

  case 'ソ':
    drawBox(K_SO, x, y);
    break;

  case 'タ':
    drawBox(K_TA, x, y);
    break;

  case 'チ':
    drawBox(K_TI, x, y);
    break;

  case 'ツ':
    drawBox(K_TU, x, y);
    break;

  case 'テ':
    drawBox(K_TE, x, y);
    break;

  case 'ト':
    drawBox(K_TO, x, y);
    break;

  case 'ナ':
    drawBox(K_NA, x, y);
    break;

  case 'ニ':
    drawBox(K_NI, x, y);
    break;

  case 'ヌ':
    drawBox(K_NU, x, y);
    break;

  case 'ネ':
    drawBox(K_NE, x, y);
    break;

  case 'ノ':
    drawBox(K_NO, x, y);
    break;

  case 'ハ':
    drawBox(K_HA, x, y);
    break;

  case 'ヒ':
    drawBox(K_HI, x, y);
    break;

  case 'フ':
    drawBox(K_HU, x, y);
    break;

  case 'ヘ':
    drawBox(K_HE, x, y);
    break;

  case 'ホ':
    drawBox(K_HO, x, y);
    break;

  case 'マ':
    drawBox(K_MA, x, y);
    break;

  case 'ミ':
    drawBox(K_MI, x, y);
    break;

  case 'ム':
    drawBox(K_MU, x, y);
    break;

  case 'メ':
    drawBox(K_ME, x, y);
    break;

  case 'モ':
    drawBox(K_MO, x, y);
    break;

  case 'ヤ':
    drawBox(K_YA, x, y);
    break;

  case 'ユ':
    drawBox(K_YU, x, y);
    break;

  case 'ヨ':
    drawBox(K_YO, x, y);
    break;

  case 'ラ':
    drawBox(K_RA, x, y);
    break;

  case 'リ':
    drawBox(K_RI, x, y);
    break;

  case 'ル':
    drawBox(K_RU, x, y);
    break;

  case 'レ':
    drawBox(K_RE, x, y);
    break;

  case 'ロ':
    drawBox(K_RO, x, y);
    break;

  case 'ワ':
    drawBox(K_WA, x, y);
    break;

  case 'ヲ':
    drawBox(K_WO, x, y);
    break;

  case 'ャ':
    drawBox(K_LYA, x, y);
    break;

  case 'ュ':
    drawBox(K_LYU, x, y);
    break;

  case 'ョ':
    drawBox(K_LYO, x, y);
    break;

  case 'ッ':
    drawBox(K_LTU, x, y);
    break;

  case 'ァ':
    drawBox(K_LA, x, y);
    break;

  case 'ィ':
    drawBox(K_LI, x, y);
    break;

  case 'ゥ':
    drawBox(K_LU, x, y);
    break;

  case 'ェ':
    drawBox(K_LE, x, y);
    break;

  case 'ォ':
    drawBox(K_LO, x, y);
    break;

  case 'ン':
    drawBox(K_NN, x, y);
    break;

  case 'ゔ':
    drawBox(DULL, x, y-1);
    drawBox(H_U, x, y);
    break;
  case 'が':
    drawBox(DULL, x, y-1);
    drawBox(H_KA, x, y);
    break;
  case 'ぎ':
    drawBox(DULL, x, y-1);
    drawBox(H_KI, x, y);
    break;
  case 'ぐ':
    drawBox(DULL, x, y-1);
    drawBox(H_KU, x, y);
    break;
  case 'げ':
    drawBox(DULL, x, y-1);
    drawBox(H_KE, x, y);
    break;
  case 'ご':
    drawBox(DULL, x, y-1);
    drawBox(H_KO, x, y);
    break;
  case 'ざ':
    drawBox(DULL, x, y-1);
    drawBox(H_SA, x, y);
    break;
  case 'じ':
    drawBox(DULL, x, y-1);
    drawBox(H_SI, x, y);
    break;
  case 'ず':
    drawBox(DULL, x, y-1);
    drawBox(H_SU, x, y);
    break;
  case 'ぜ':
    drawBox(DULL, x, y-1);
    drawBox(H_SE, x, y);
    break;
  case 'ぞ':
    drawBox(DULL, x, y-1);
    drawBox(H_SO, x, y);
    break;
  case 'だ':
    drawBox(DULL, x, y-1);
    drawBox(H_TA, x, y);
    break;
  case 'ぢ':
    drawBox(DULL, x, y-1);
    drawBox(H_TI, x, y);
    break;
  case 'づ':
    drawBox(DULL, x, y-1);
    drawBox(H_TU, x, y);
    break;
  case 'で':
    drawBox(DULL, x, y-1);
    drawBox(H_TE, x, y);
    break;
  case 'ど':
    drawBox(DULL, x, y-1);
    drawBox(H_TO, x, y);
    break;
  case 'ば':
    drawBox(DULL, x, y-1);
    drawBox(H_HA, x, y);
    break;
  case 'び':
    drawBox(DULL, x, y-1);
    drawBox(H_HI, x, y);
    break;
  case 'ぶ':
    drawBox(DULL, x, y-1);
    drawBox(H_HU, x, y);
    break;
  case 'べ':
    drawBox(DULL, x, y-1);
    drawBox(H_HE, x, y);
    break;
  case 'ぼ':
    drawBox(DULL, x, y-1);
    drawBox(H_HO, x, y);
    break;
  case 'ぱ':
    drawBox(POP, x, y-1);
    drawBox(H_HA, x, y);
    break;
  case 'ぴ':
    drawBox(POP, x, y-1);
    drawBox(H_HI, x, y);
    break;
  case 'ぷ':
    drawBox(POP, x, y-1);
    drawBox(H_HU, x, y);
    break;
  case 'ぺ':
    drawBox(POP, x, y-1);
    drawBox(H_HE, x, y);
    break;
  case 'ぽ':
    drawBox(POP, x, y-1);
    drawBox(H_HO, x, y);
    break;

  case 'ヴ':
    drawBox(DULL, x, y-1);
    drawBox(K_U, x, y);
    break;
  case 'ガ':
    drawBox(DULL, x, y-1);
    drawBox(K_KA, x, y);
    break;
  case 'ギ':
    drawBox(DULL, x, y-1);
    drawBox(K_KI, x, y);
    break;
  case 'グ':
    drawBox(DULL, x, y-1);
    drawBox(K_KU, x, y);
    break;
  case 'ゲ':
    drawBox(DULL, x, y-1);
    drawBox(K_KE, x, y);
    break;
  case 'ゴ':
    drawBox(DULL, x, y-1);
    drawBox(K_KO, x, y);
    break;
  case 'ザ':
    drawBox(DULL, x, y-1);
    drawBox(K_SA, x, y);
    break;
  case 'ジ':
    drawBox(DULL, x, y-1);
    drawBox(K_SI, x, y);
    break;
  case 'ズ':
    drawBox(DULL, x, y-1);
    drawBox(K_SU, x, y);
    break;
  case 'ゼ':
    drawBox(DULL, x, y-1);
    drawBox(K_SE, x, y);
    break;
  case 'ゾ':
    drawBox(DULL, x, y-1);
    drawBox(K_SO, x, y);
    break;
  case 'ダ':
    drawBox(DULL, x, y-1);
    drawBox(K_TA, x, y);
    break;
  case 'ヂ':
    drawBox(DULL, x, y-1);
    drawBox(K_TI, x, y);
    break;
  case 'ヅ':
    drawBox(DULL, x, y-1);
    drawBox(K_TU, x, y);
    break;
  case 'デ':
    drawBox(DULL, x, y-1);
    drawBox(K_TE, x, y);
    break;
  case 'ド':
    drawBox(DULL, x, y-1);
    drawBox(K_TO, x, y);
    break;
  case 'バ':
    drawBox(DULL, x, y-1);
    drawBox(K_HA, x, y);
    break;
  case 'ビ':
    drawBox(DULL, x, y-1);
    drawBox(K_HI, x, y);
    break;
  case 'ブ':
    drawBox(DULL, x, y-1);
    drawBox(K_HU, x, y);
    break;
  case 'ベ':
    drawBox(DULL, x, y-1);
    drawBox(K_HE, x, y);
    break;
  case 'ボ':
    drawBox(DULL, x, y-1);
    drawBox(K_HO, x, y);
    break;
  case 'パ':
    drawBox(POP, x, y-1);
    drawBox(K_HA, x, y);
    break;
  case 'ピ':
    drawBox(POP, x, y-1);
    drawBox(K_HI, x, y);
    break;
  case 'プ':
    drawBox(POP, x, y-1);
    drawBox(K_HU, x, y);
    break;
  case 'ペ':
    drawBox(POP, x, y-1);
    drawBox(K_HE, x, y);
    break;
  case 'ポ':
    drawBox(POP, x, y-1);
    drawBox(K_HO, x, y);
    break;
  case '。':
    drawBox(PELI, x, y);
    break;



    //  case 'A':
    //    drawBox(A, x, y);
    //    break;

    //  case 'B':
    //    drawBox(B, x, y);
    //    break;

    //  case 'C':
    //    drawBox(C, x, y);
    //    break;

    //  case 'D':
    //    drawBox(D, x, y);
    //    break;

    //  case 'E':
    //    drawBox(E, x, y);
    //    break;

    //  case 'F':
    //    drawBox(F, x, y);
    //    break;

    //  case 'G':
    //    drawBox(G, x, y);
    //    break;

    //  case 'H':
    //    drawBox(H, x, y);
    //    break;

    //  case 'X':
    //    drawBox(X, x, y);
    //    break;

    //  case 'V':
    //    drawBox(V, x, y);
    //    break;

    //  case 'L':
    //    drawBox(L, x, y);
    //    break;

    //  case 'M':
    //    drawBox(M, x, y);
    //    break;

    //  case 'P':
    //    drawBox(P, x, y);
    //    break;


  case '0':
    drawBox(N_0, x, y);
    break;

  case '1':
    drawBox(N_1, x, y);
    break;

  case '2':
    drawBox(N_2, x, y);
    break;

  case '3':
    drawBox(N_3, x, y);
    break;

  case '4':
    drawBox(N_4, x, y);
    break;
  case '5':
    drawBox(N_5, x, y);
    break;

  case '6':
    drawBox(N_6, x, y);
    break;

  case '7':
    drawBox(N_7, x, y);
    break;

  case '8':
    drawBox(N_8, x, y);
    break;

  case '9':
    drawBox(N_9, x, y);
    break;

  case '.':
    drawBox(POIN, x, y);
    break;



    //  case '*':
    //    drawBox(ASTA, x, y);
    //    break;

    //  case '>':
    //    drawBox(ALLOW, x, y);
    //    break;

    //  case ':':
    //    drawBox(DEPE, x, y);
    //    break;

    //  case '「':
    //    drawBox(PARE, x, y);
    //    break;

  case 'ー':
    drawBox(HIHN, x, y);
    break;

    //  case '！':
    //    drawBox(EXEC, x, y);
    //    break;

    //  case '？':
    //    drawBox(QUES, x, y);
    //    break;

    //  case '。':
    //    drawBox(PERI, x, y);
    //    break;

    //  case '、':
    //    drawBox(KOMM, x, y);
    //    break;

    //  case '↓':
    //    drawBox(PUSH, x, y);
    //    break;

    //  case '→':
    //    drawBox(SELE, x, y);
    //    break;

  case '　':
    break;
  }
}
