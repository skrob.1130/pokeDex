void Pokedex(){
  //枠線
  for(int i = 1; i <= 18; i++){
    drawBox(PD_GAP_T, i, 0);
    drawBox(PD_GAP_B, i, 17);
  }
  for(int i = 1; i <= 8; i++){
    drawBox(PD_GAP_L, 0, i);
    drawBox(PD_GAP_R, 19, i);
  }
  for(int i = 10; i <= 16; i++){
    drawBox(PD_GAP_L, 0, i);
    drawBox(PD_GAP_R, 19, i);
  }
  drawBox(PD_GAP_LT, 0, 0);
  drawBox(PD_GAP_RT, 19, 0);
  drawBox(PD_GAP_LB, 0, 17);
  drawBox(PD_GAP_RB, 19, 17);
  drawBox(PD_GAP_LC, 0, 9);
  drawBox(PD_GAP_RC, 19, 9);
  
  //綴じ部分
  drawBox(PD_GAP_CRING, 1, 9);
  drawBox(PD_GAP_CB, 2, 9);
  drawBox(PD_GAP_CRING, 3, 9);
  drawBox(PD_GAP_CB, 4, 9);
  drawBox(PD_GAP_CRING, 5, 9);
  drawBox(PD_GAP_CB, 6, 9);
  drawBox(PD_GAP_CRING, 7, 9);
  drawBox(PD_GAP_CB, 8, 9);
  drawBox(PD_GAP_CB, 9, 9);
  drawBox(PD_GAP_CB, 10, 9);
  drawBox(PD_GAP_CB, 11, 9);
  drawBox(PD_GAP_CRING, 12, 9);
  drawBox(PD_GAP_CB, 13, 9);
  drawBox(PD_GAP_CRING, 14, 9);
  drawBox(PD_GAP_CB, 15, 9);
  drawBox(PD_GAP_CRING, 16, 9);
  drawBox(PD_GAP_CB, 17, 9);
  drawBox(PD_GAP_CRING, 18, 9);
  
  //情報
  drawBox(NO, 2, 8);
  drawBox(POIN, 3, 8);
  drawBox(POIN, 15, 8);
  drawBox(m, 17, 6);
  drawBox(POIN, 15, 6);
  drawBox(k, 17, 8);
  drawBox(g, 18, 8);
  drawBox(H_TA, 9, 6);
  drawBox(H_KA, 10, 6);
  drawBox(H_SA, 11, 6);
  drawBox(H_O, 9, 8);
  drawBox(H_MO, 10, 8);
  drawBox(H_SA, 11, 8);
}
