

byte[] DULL;
byte[] EXEC;
byte[] LV;
byte[] NO;
byte[] PELI;
byte[] POIN;
byte[] POP;
byte[] SLSH;//スラッシュ
byte[] HIHN ;//ハイフン
byte[] HP_1 ;
byte[] HP_2 ;
byte[] ID ;

//ひらがな
byte[] H_A;
byte[] H_I ;
byte[] H_U;
byte[] H_E ;
byte[] H_O ;
byte[] H_KA;
byte[] H_KI ;
byte[] H_KU;
byte[] H_KE ;
byte[] H_KO;
byte[] H_SA ;
byte[] H_SI ;
byte[] H_SU ;
byte[] H_SE ;
byte[] H_SO ;
byte[] H_TA ;
byte[] H_TI ;
byte[] H_TU ;
byte[] H_TE ;
byte[] H_TO ;
byte[] H_NA;
byte[] H_NI ;
byte[] H_NU ;
byte[] H_NE;
byte[] H_NO ;
byte[] H_HA;
byte[] H_HI ;
byte[] H_HU ;
byte[] H_HE;
byte[] H_HO ;
byte[] H_MA;
byte[] H_MI ;
byte[] H_MU ;
byte[] H_ME;
byte[] H_MO ;
byte[] H_RA ;
byte[] H_RI;
byte[] H_RU ;
byte[] H_RE;
byte[] H_RO ;
byte[] H_LA;
byte[] H_LI ;
byte[] H_LU ;
byte[] H_LE ;
byte[] H_LO ;
byte[] H_LKA ;
byte[] H_LKE;
byte[] H_LWA ;
byte[] H_LYA;
byte[] H_LYU;
byte[] H_LYO;
byte[] H_WA;
byte[] H_WI ;
byte[] H_WO;
byte[] H_WE;
byte[] H_YA ;
byte[] H_YU ;
byte[] H_YO;
byte[] H_NN;
byte[] H_LTU;

//カタカナ
byte[] K_A ;
byte[] K_I ;
byte[] K_U ;
byte[] K_E ;
byte[] K_O ;
byte[] K_KA ;
byte[] K_KI ;
byte[] K_KU ;
byte[] K_KE ;
byte[] K_KO;
byte[] K_SA;
byte[] K_SI;
byte[] K_SU ;
byte[] K_SE ;
byte[] K_SO;
byte[] K_TA;
byte[] K_TI ;
byte[] K_TU ;
byte[] K_TE ;
byte[] K_TO;
byte[] K_NA ;
byte[] K_NI;
byte[] K_NU;
byte[] K_NE;
byte[] K_NO ;
byte[] K_HA ;
byte[] K_HI ;
byte[] K_HU;
byte[] K_HE ;
byte[] K_HO ;
byte[] K_MA ;
byte[] K_MI;
byte[] K_MU;
byte[] K_ME ;
byte[] K_MO ;
byte[] K_RA ;
byte[] K_RI;
byte[] K_RU ;
byte[] K_RE ;
byte[] K_RO;
byte[] K_LA ;
byte[] K_LI;
byte[] K_LE;
byte[] K_LU ;
byte[] K_LO;
byte[] K_LKA;
byte[] K_LKE;
byte[] K_LWA ;
byte[] K_LYA;
byte[] K_LYU;
byte[] K_LYO;
byte[] K_YA ;
byte[] K_YU ;
byte[] K_YO;
byte[] K_WA;
byte[] K_WI ;
byte[] K_WE ;
byte[] K_WO ;
byte[] K_NN;
byte[] K_LTU;

//ポケモン図鑑
byte[] g;
byte[] k;
byte[] m;
byte[] PD_GAP_B ;//下のライン
byte[] PD_GAP_CRING ;//綴じ
byte[] PD_GAP_L ;//左のライン
byte[] PD_GAP_LB ;//左下
byte[] PD_GAP_LC ;//左綴じ
byte[] PD_GAP_LT ;//左上
byte[] PD_GAP_R;//右のライン
byte[] PD_GAP_RB ;//右下
byte[] PD_GAP_RC ;//右綴じ
byte[] PD_GAP_RT ;//右上
byte[] PD_GAP_T ;//上のライン
byte[] PD_GAP_CB ;

byte[] BW_ALLOW_L ;
byte[] BW_ALLOW_R ;
byte[] BW_B ;
byte[] BW_BB ;
byte[] BW_LB ;
byte[] BW_RB ;

byte[] N_0 ;
byte[] N_1 ;
byte[] N_2 ;
byte[] N_3 ;
byte[] N_4 ;
byte[] N_5 ;
byte[] N_6 ;
byte[] N_7 ;
byte[] N_8 ;
byte[] N_9 ;
void charInput() {
  //記号達
  DULL = loadBytes("text/DULL.dat");
  EXEC = loadBytes("text/EXEC.dat");
  LV = loadBytes("text/LV.dat");
  NO = loadBytes("text/NO.dat");
  PELI = loadBytes("text/PELI.dat");
  POIN = loadBytes("text/POINT.dat");
  POP = loadBytes("text/POP.dat");
  SLSH = loadBytes("text/SLSH.dat");//スラッシュ
  HIHN = loadBytes("text/HIHN.dat");//ハイフン
  HP_1 = loadBytes("text/HP_1.dat");
  HP_2 = loadBytes("text/HP_2.dat");
  ID = loadBytes("text/ID.dat");

  //ひらがな
  H_A = loadBytes("text/H_A.dat");
  H_I = loadBytes("text/H_I.dat");
  H_U = loadBytes("text/H_U.dat");
  H_E = loadBytes("text/H_E.dat");
  H_O = loadBytes("text/H_O.dat");
  H_KA = loadBytes("text/H_KA.dat");
  H_KI = loadBytes("text/H_KI.dat");
  H_KU = loadBytes("text/H_KU.dat");
  H_KE = loadBytes("text/H_KE.dat");
  H_KO = loadBytes("text/H_KO.dat");
  H_SA = loadBytes("text/H_SA.dat");
  H_SI = loadBytes("text/H_SI.dat");
  H_SU = loadBytes("text/H_SU.dat");
  H_SE = loadBytes("text/H_SE.dat");
  H_SO = loadBytes("text/H_SO.dat");
  H_TA = loadBytes("text/H_TA.dat");
  H_TI = loadBytes("text/H_TI.dat");
  H_TU = loadBytes("text/H_TU.dat");
  H_TE = loadBytes("text/H_TE.dat");
  H_TO = loadBytes("text/H_TO.dat");
  H_NA = loadBytes("text/H_NA.dat");
  H_NI = loadBytes("text/H_NI.dat");
  H_NU = loadBytes("text/H_NU.dat");
  H_NE = loadBytes("text/H_NE.dat");
  H_NO = loadBytes("text/H_NO.dat");
  H_HA = loadBytes("text/H_HA.dat");
  H_HI = loadBytes("text/H_HI.dat");
  H_HU = loadBytes("text/H_HU.dat");
  H_HE = loadBytes("text/H_HE.dat");
  H_HO = loadBytes("text/H_HO.dat");
  H_MA = loadBytes("text/H_MA.dat");
  H_MI = loadBytes("text/H_MI.dat");
  H_MU = loadBytes("text/H_MU.dat");
  H_ME = loadBytes("text/H_ME.dat");
  H_MO = loadBytes("text/H_MO.dat");
  H_RA = loadBytes("text/H_RA.dat");
  H_RI = loadBytes("text/H_RI.dat");
  H_RU = loadBytes("text/H_RU.dat");
  H_RE = loadBytes("text/H_RE.dat");
  H_RO = loadBytes("text/H_RO.dat");
  H_LA = loadBytes("text/H_LA.dat");
  H_LI = loadBytes("text/H_LI.dat");
  H_LU = loadBytes("text/H_LU.dat");
  H_LE = loadBytes("text/H_LE.dat");
  H_LO = loadBytes("text/H_LO.dat");
  H_LKA = loadBytes("text/H_LKA.dat");
  H_LKE = loadBytes("text/H_LKE.dat");
  H_LWA = loadBytes("text/H_LWA.dat");
  H_LYA = loadBytes("text/H_LYA.dat");
  H_LYU = loadBytes("text/H_LYU.dat");
  H_LYO = loadBytes("text/H_LYO.dat");
  H_WA = loadBytes("text/H_WA.dat");
  H_WI = loadBytes("text/H_WI.dat");
  H_WO = loadBytes("text/H_WO.dat");
  H_WE = loadBytes("text/H_WE.dat");
  H_YA = loadBytes("text/H_YA.dat");
  H_YU = loadBytes("text/H_YU.dat");
  H_YO = loadBytes("text/H_YO.dat");
  H_NN = loadBytes("text/H_NN.dat");
  H_LTU = loadBytes("text/H_LTU.dat");

  //カタカナ
  K_A = loadBytes("text/K_A.dat");
  K_I = loadBytes("text/K_I.dat");
  K_U = loadBytes("text/K_U.dat");
  K_E = loadBytes("text/K_E.dat");
  K_O = loadBytes("text/K_O.dat");
  K_KA = loadBytes("text/K_KA.dat");
  K_KI = loadBytes("text/K_KI.dat");
  K_KU = loadBytes("text/K_KU.dat");
  K_KE = loadBytes("text/K_KE.dat");
  K_KO = loadBytes("text/K_KO.dat");
  K_SA = loadBytes("text/K_SA.dat");
  K_SI = loadBytes("text/K_SI.dat");
  K_SU = loadBytes("text/K_SU.dat");
  K_SE = loadBytes("text/K_SE.dat");
  K_SO = loadBytes("text/K_SO.dat");
  K_TA = loadBytes("text/K_TA.dat");
  K_TI = loadBytes("text/K_TI.dat");
  K_TU = loadBytes("text/K_TU.dat");
  K_TE = loadBytes("text/K_TE.dat");
  K_TO = loadBytes("text/K_TO.dat");
  K_NA = loadBytes("text/K_NA.dat");
  K_NI = loadBytes("text/K_NI.dat");
  K_NU = loadBytes("text/K_NU.dat");
  K_NE = loadBytes("text/K_NE.dat");
  K_NO = loadBytes("text/K_NO.dat");
  K_HA = loadBytes("text/K_HA.dat");
  K_HI = loadBytes("text/K_HI.dat");
  K_HU = loadBytes("text/K_HU.dat");
  K_HE = loadBytes("text/K_HE.dat");
  K_HO = loadBytes("text/K_HO.dat");
  K_MA = loadBytes("text/K_MA.dat");
  K_MI = loadBytes("text/K_MI.dat");
  K_MU = loadBytes("text/K_MU.dat");
  K_ME = loadBytes("text/K_ME.dat");
  K_MO = loadBytes("text/K_MO.dat");
  K_RA = loadBytes("text/K_RA.dat");
  K_RI = loadBytes("text/K_RI.dat");
  K_RU = loadBytes("text/K_RU.dat");
  K_RE = loadBytes("text/K_RE.dat");
  K_RO = loadBytes("text/K_RO.dat");
  K_LA = loadBytes("text/K_LA.dat");
  K_LI = loadBytes("text/K_LI.dat");
  K_LE = loadBytes("text/K_LE.dat");
  K_LU = loadBytes("text/K_LU.dat");
  K_LO = loadBytes("text/K_LO.dat");
  K_LKA = loadBytes("text/K_LKA.dat");
  K_LKE = loadBytes("text/K_LKE.dat");
  K_LWA = loadBytes("text/K_LWA.dat");
  K_LYA = loadBytes("text/K_LYA.dat");
  K_LYU = loadBytes("text/K_LYU.dat");
  K_LYO = loadBytes("text/K_LYO.dat");
  K_YA = loadBytes("text/K_YA.dat");
  K_YU = loadBytes("text/K_YU.dat");
  K_YO = loadBytes("text/K_YO.dat");
  K_WA = loadBytes("text/K_WA.dat");
  K_WI = loadBytes("text/K_WI.dat");
  K_WE = loadBytes("text/K_WE.dat");
  K_WO = loadBytes("text/K_WO.dat");
  K_NN = loadBytes("text/K_NN.dat");
K_LTU = loadBytes("text/K_LTU.dat");

  //ポケモン図鑑
  g = loadBytes("Pokedex/g.dat");
  k = loadBytes("Pokedex/k.dat");
  m = loadBytes("Pokedex/m.dat");
  PD_GAP_B = loadBytes("Pokedex/PD_GAP_B.dat");
  PD_GAP_CRING = loadBytes("Pokedex/PD_GAP_CRING.dat");
  PD_GAP_L = loadBytes("Pokedex/PD_GAP_L.dat");
  PD_GAP_LB = loadBytes("Pokedex/PD_GAP_LB.dat");
  PD_GAP_LC = loadBytes("Pokedex/PD_GAP_LC.dat");
  PD_GAP_LT = loadBytes("Pokedex/PD_GAP_LT.dat");
  PD_GAP_R = loadBytes("Pokedex/PD_GAP_R.dat");
  PD_GAP_RB = loadBytes("Pokedex/PD_GAP_RB.dat");
  PD_GAP_RC = loadBytes("Pokedex/PD_GAP_RC.dat");
  PD_GAP_RT = loadBytes("Pokedex/PD_GAP_RT.dat");
  PD_GAP_T = loadBytes("Pokedex/PD_GAP_T.dat");
  PD_GAP_CB = loadBytes("Pokedex/PD_GAP_CB.dat");

  BW_ALLOW_L = loadBytes("BattleWindow/BW_ALLOW_L.dat");
  BW_ALLOW_R  = loadBytes("BattleWindow/BW_ALLOW_R.dat");
  BW_B  = loadBytes("BattleWindow/BW_B.dat");
  BW_BB  = loadBytes("BattleWindow/BW_BB.dat");
  BW_LB  = loadBytes("BattleWindow/BW_LB.dat");
  BW_RB  = loadBytes("BattleWindow/BW_RB.dat");
  
  N_1 = loadBytes("window/N_1.dat");
  N_2 = loadBytes("window/N_2.dat");
  N_3 = loadBytes("window/N_3.dat");
  N_4 = loadBytes("window/N_4.dat");
  N_5 = loadBytes("window/N_5.dat");
  N_6 = loadBytes("window/N_6.dat");
  N_7 = loadBytes("window/N_7.dat");
  N_8 = loadBytes("window/N_8.dat");
  N_9 = loadBytes("window/N_9.dat");
  N_0 = loadBytes("window/N_0.dat");
}
