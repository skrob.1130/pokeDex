class PokemonData {
  char[] id;
  char[] name;
  char[] race;
  char[] w;
  char[] h;
  char[] explain_1;
  char[] explain_2;
  char[] explain_3;
  byte[] dotData;

  PokemonData(String filename, String dotFile) {
    String[] lines = loadStrings(filename);
    dotData = loadBytes(dotFile);
    id = new char[3];
    id = splitText(lines[0]);
    name = splitText(lines[1]);
    race = splitText(lines[2] + "ポケモン");
    w = splitText(lines[3]);
    h = splitText(lines[4]);
    explain_1 = splitText(lines[5]);
    explain_2 = splitText(lines[6]);
    explain_3 = splitText(lines[7]);
    id = reverse(id);
    id = append(id, '0');
    w = reverse(w);
    h = reverse(h);
  }
  
  void displayInfo(){
    //名称
    for(int i = 0; i < name.length; i++){
      drawChar(name[i], 9+i, 2);
    }
    //種族
    for(int i = 0; i < race.length; i++){
      drawChar(race[i], 9+i, 4);
    }
    //高さ
    for(int i = w.length-1; i >= 0; i --){
      drawChar(w[i], 16-i, 6);
    }
    //重さ
    for(int i = h.length-1; i >= 0; i --){
      drawChar(h[i], 16-i, 8);
    }
    //番号
    for(int i = id.length-1; i >= 0; i --){
      drawChar(id[i], 6-i, 8);
    }
    //説明文
    for(int i = 0; i < explain_1.length; i++){
      drawChar(explain_1[i], 1+i, 11);
    }
    for(int i = 0; i < explain_2.length; i++){
      drawChar(explain_2[i], 1+i, 13);
    }
    for(int i = 0; i < explain_3.length; i++){
      drawChar(explain_3[i], 1+i, 15);
    }
    //イメージ
    switch(dotData.length){
      case 1600:
        for(int j = 0; j < 40; j++){
          for(int i = 0; i < 40; i++){
            fill(sepia[dotData[j*40 + i]]);
            rect(2*boxSize*dotSize+i*dotSize, 3*boxSize*dotSize+j*dotSize, dotSize, dotSize);
          }
        }
        break;
        
       case 2304:
          for(int j = 0; j < 40; j++){
            for(int i = 0; i < 40; i++){
              fill(sepia[dotData[j*40 + i]]);
              rect(1*boxSize*dotSize+i*dotSize, 2*boxSize*dotSize+j*dotSize, dotSize, dotSize);
            }
          }
          break;
       case 3136:
          for(int j = 0; j < 40; j++){
            for(int i = 0; i < 40; i++){
              fill(sepia[dotData[j*40 + i]]);
              rect(1*boxSize*dotSize+i*dotSize, 1*boxSize*dotSize+j*dotSize, dotSize, dotSize);
            }
          }
          break;
          default:
          break;
    }
  }
}
