int boxX = 20;
int boxY = 18;
int dotSize = 3;
int boxSize = 8;

color back = color(206, 206, 172);

PokemonData pikachu;

color[] sepia = {
  color(206, 206, 172),
  color(164, 164, 141),
  color(110, 110, 85),
  color(36, 36, 15)
};

void settings(){
  size(boxX*dotSize*boxSize, boxY*dotSize*boxSize);
}

void setup(){
  noStroke();
  frameRate(60);
  charInput();
  pikachu = new PokemonData("test.txt", "pikachu.dat");
}

void draw(){
  background(back);
  Pokedex();
  pikachu.displayInfo();
}

void drawBox(byte[] compo, int x, int y){
  for(int j = 0; j < 8; j++){
    for(int i = 0; i < 8; i++){
      fill(sepia[compo[j*8 + i]]);
      rect(x*boxSize*dotSize+i*dotSize, y*boxSize*dotSize+j*dotSize, dotSize, dotSize);
    }
  }
}

char[] splitText(String text) {
  char[] work = new char[text.length()];
  for (int i= 0; i < text.length(); i++) {
    work[i] = text.charAt(i);
  }
  return work;
}
